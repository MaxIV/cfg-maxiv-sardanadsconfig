import json
from jsonschema import validate
from os.path import join, abspath, dirname
import subprocess
import pytest


@pytest.fixture
def generate_conf():
    # make the initial config
    config = subprocess.check_output(["python3", "make_sardana_dsconfig.py", "test/MaxPEEM_Sardana_test_initial.xlsx", "test/dummymotorsheet.xlsx"])
    result_data = json.loads(config)
    return result_data


def test_sardana_jsonschema(generate_conf):
    schema_file = open(join(dirname(abspath(__file__)), "dsconfig_schema.json"))
    schema_data = json.loads(schema_file.read())
    return validate(generate_conf, schema_data)


def test_sardanasheet_motor_poslimit(generate_conf):
    assert generate_conf['servers']['Pool']['B111A']['Motor']["B111A-FE/OPT/MM-01-X"]["attribute_properties"]["Position"]["max_value"][0] == "5.038559"


def test_sardanasheet_motor_neglimit(generate_conf):
    assert generate_conf['servers']['Pool']['B111A']['Motor']["B111A-FE/OPT/MM-01-X"]["attribute_properties"]["Position"]["min_value"][0] == "-5.304458"


def test_motorsheet_motor_poslimit(generate_conf):
    assert generate_conf['servers']['Pool']['B111A']['Motor']["b111a-o01/opt/dummy-01-hlat"]["attribute_properties"]["Position"]["max_value"][0] == "8.0"


def test_motorsheet_motor_neglimit(generate_conf):
    assert generate_conf['servers']['Pool']['B111A']['Motor']["b111a-o01/opt/dummy-01-hlat"]["attribute_properties"]["Position"]["min_value"][0] == "-8.0"