import json
import subprocess
import pytest

expected_initial_output_filename = "./test/MaxPEEM_Sardana_test_initial.json"
sardana_xls_initial_name = "./test/MaxPEEM_Sardana_test_initial.xlsx"
sardana_xls_update_name = "./test/MaxPEEM_Sardana_test_update.xlsx"

expected_initial_output_file = open(expected_initial_output_filename)
expected_initial_output_data = json.loads(expected_initial_output_file.read())


@pytest.fixture
def generate_conf_initial():
    # make the initial config
    config_initial = subprocess.check_output(["python3", "make_sardana_dsconfig.py", sardana_xls_initial_name])
    result_initial_data = json.loads(config_initial)
    return result_initial_data


@pytest.fixture
def generate_conf_update():
    # generate an updated configuration, using the stored expected output as input
    config_update = subprocess.check_output(["python3", "make_sardana_dsconfig.py", sardana_xls_update_name, "-u", expected_initial_output_filename])
    result_update_data = json.loads(config_update)
    return result_update_data


def compare_keys_recursive(new_dict, expected_dict, dict_path):
    new_keys = new_dict.keys()
    expected_keys = expected_dict.keys()
    for expected_key in expected_keys:
        new_path = dict_path + "/" + expected_key
        print("Checking key: {}".format(new_path))
        assert expected_key in new_keys
        if isinstance(expected_dict[expected_key], dict):
            compare_keys_recursive(new_dict[expected_key], expected_dict[expected_key], new_path)
        else:
            print("Checking values, expected: {}, got: {}".format(expected_dict[expected_key], new_dict[expected_key]))
            assert new_dict[expected_key] == expected_dict[expected_key]


def compare_id_recursive(new_dict, expected_dict, dict_path):
    new_keys = new_dict.keys()
    expected_keys = expected_dict.keys()
    for expected_key in expected_keys:
        new_path = dict_path + "/" + expected_key
        print("Checking key: {}".format(new_path))
        assert expected_key in new_keys
        if isinstance(expected_dict[expected_key], dict):
            compare_id_recursive(new_dict[expected_key], expected_dict[expected_key], new_path)
        else:
            if expected_key == "id":
                print("Checking id, expected: {}, got: {}".format(expected_dict[expected_key], new_dict[expected_key]))
                assert new_dict[expected_key] == expected_dict[expected_key]


def test_initial_conf(generate_conf_initial):
    # Check that all data is there
    compare_keys_recursive(generate_conf_initial, expected_initial_output_data, "")


def test_update_conf(generate_conf_update):
    # Check that the update didn't change any ids
    compare_id_recursive(generate_conf_update, expected_initial_output_data, "")
