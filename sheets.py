import logging

from utils import iter_rows, normalize_header
import sys


def check_motor_sheet(
    book,
    page,
    colmap=None,
    xls_filename=None,
    required_cols=[],
    optional_cols=[],
):
    logger = logging.getLogger("{}[{}]".format(xls_filename, page))
    sheet = book.sheet_by_name(page)
    rows = sheet.row_values(0)
    columns = [normalize_header(v) for v in rows]
    if colmap:
        columns = [colmap.get(value, value) for value in columns]

    sheet_ok = True
    for col in required_cols:
        if col not in columns:
            logger.warning("Required column {} not found!".format(col))
            sheet_ok = False
    for col in optional_cols:
        if col not in columns:
            logger.warning("Optional column {} not found!".format(col))
    return sheet_ok


def process_motor_sheet(
    book,
    page,
    elements,
    aliases,
    instruments,
    controllers,
    pools,
    servers,
    get_id,
    colmap=None,
    default_type="Motor",
    n_columns=0,
    xls_filename=None,
    use_column=None,
):

    """
    Read a motor sheet and update the configuration from it.
    This includes pseudomotors, ioregisters, ...
    In order to work with sheets from other sources, it's possible to
    include a mapping of column names in "colmap".
    if use_column is set to a column name, all rows for which that
    column is empty will be skipped.
    """

    logger = logging.getLogger("{}[{}]".format(xls_filename, page))

    sheet = book.sheet_by_name(page)

    if sheet.ncols == 0 or sheet.nrows == 0:
        # empty sheet...
        logger.info("Empty sheet!")
        return

    for i, columns in enumerate(iter_rows(sheet, n_columns=n_columns)):
        if colmap:
            columns = {
                colmap.get(key, key): value for key, value in columns.items()
            }

        if use_column:
            if use_column not in columns.keys():
                # the "use" column is not found
                logger.fatal(
                    "Exiting because column '%s' was not found", use_column
                )
                sys.exit()
            elif not columns.get(use_column.lower()):
                # ignore the line if the "use" column is empty
                logger.debug(
                    "Ignoring row %d because column '%s' is empty",
                    i,
                    use_column,
                )
                continue
            elif (
                columns.get(use_column.lower()).lower() != "y"
                and columns.get(use_column.lower()).lower() != "yes"
            ):
                # ignore the line if the "use" column is not "Y"
                logger.debug(
                    "Ignoring row %d because column '%s' is not 'y' or 'yes'",
                    i,
                    use_column,
                )
                continue

        # sanity checking
        if not columns["name"] or not columns["controller"]:
            continue

        try:
            controller = elements[columns["controller"]]
        except KeyError:
            logger.warning(
                (
                    "Could not find controller '{controller}' for {name}"
                    " (motor sheet {sheet.name}, row {i})"
                ).format(i=i + 1, sheet=sheet, **columns)
            )
            continue

        if columns["name"] in aliases:
            logger.warning(
                (
                    "Alias {name} already taken by {rival}!"
                    " (motor sheet {sheet.name}, row {i})"
                ).format(
                    i=i + 1,
                    sheet=sheet,
                    rival=aliases[columns["name"]],
                    **columns
                )
            )
            continue

        # figure out a device name
        element_type = columns.get("type", default_type)
        if (
            columns["devicename"]
            and columns["devicename"].lower() != "automatic"
        ):
            devicename = columns["devicename"].strip()
        else:
            # none given, generate a name (maybe we should just skip?)
            devicename = "{element_type}/{controller}/{axis:.0f}".format(
                element_type=element_type, **columns
            ).lower()
        aliases[columns["name"]] = devicename
        element_id = get_id(devicename)

        device = {
            "alias": columns["name"],
            "properties": {
                "id": [element_id],
                "axis": [str(int(columns["axis"]))],
                "ctrl_id": controller["properties"]["id"],
            },
        }

        instrument = columns.get("instrument")
        if instrument in instruments:
            device["properties"]["instrument_id"] = [
                str(instruments[instrument])
            ]

        # To set attribute values we need to write to the properties
        # used for storing memorized attribute values: __value
        if columns.get("attributes"):
            aps = device.setdefault("attribute_properties", {})
            for prop in columns["attributes"].split(";"):
                if not prop:
                    continue
                attr, value = prop.split(":", 1)
                if not value:
                    # Don't add empty properties (we never need those right?)
                    continue

                # TANGO wants memorized booleans to be lowercased (?)
                if value.lower() in ("true", "false"):
                    value = [value.lower()]
                else:
                    value = value.strip().split("\n")

                # TODO: perhaps do more sanity checking here, that
                # the values make sense, at least that the type is
                # correct. But that requires knowledge of all
                # possible motor device types.  Note: perhaps it
                # makes more sense to do this through the json
                # schema, to separate concerns a bit?

                aps[attr.strip()] = {"__value": value}

        # Allow the most important Position config parameters to be set
        # here instead of the "Parameters" page in the sardana sheet.
        for param in ["unit", "min_value", "max_value"]:
            if columns.get(param):
                # pos = (device.setdefault("attribute_properties", {})
                #       .setdefault("Position", {}))
                param_val = str(columns[param])
                # if params:
                if "attribute_properties" not in device:
                    device["attribute_properties"] = {}
                if "Position" not in device["attribute_properties"]:
                    device["attribute_properties"]["Position"] = {}
                device["attribute_properties"]["Position"][param] = [param_val]

        # Figure out some connections for pseudomotors
        if element_type == "PseudoMotor":
            # Note: why does each pseudomotor axis need to know what motor
            # roles are involved? Isn't this the controller's job?
            device["properties"]["elements"] = controller["properties"].get(
                "motor_role_ids", []
            )

        # finally add the motor device to the config
        pool = controllers[columns["controller"]]
        srv, inst = pools[pool or ""]
        if element_type in servers[srv][inst]:
            servers[srv][inst][element_type][devicename] = device
        else:
            servers[srv][inst][element_type] = {devicename: device}

        elements[columns["name"]] = device
