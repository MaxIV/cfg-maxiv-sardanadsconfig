import logging
import re


def normalize_header(header):
    """Make a header string more predictable, by removing non-alphanumerics,
    replacing + with "pos" and - with "neg",
    lowercasing and replacing spaces with underscores.
    Also handles some common differences in column names."""
    sanitized = re.sub(r'[+]', 'pos', header)
    sanitized = re.sub(r'[-]', 'neg', sanitized)
    sanitized = re.sub(r'[^\w\s\d]', '', sanitized)
    # The ready column often has a lot of extra text that varies from sheet to sheet..
    if sanitized.lower().startswith("ready"):
        sanitized = "ready"
    elif sanitized.lower() == "units":
        sanitized = "unit"
    return re.sub(r"\s+", "_", sanitized.strip().lower())


def iter_rows(page, start_row=1, header_row=0,
              required_columns=3, n_columns=0):
    """
    Iterator over the rows in a page, yielding dicts of
    column header, value pairs.
    One row (first by default) is assumed to contain column
    headers.
    Only the first n_columns columns are used (if non-zero)
    """

    if n_columns > 0:
        rows = page.row_values(header_row)[:n_columns]
    else:
        rows = page.row_values(header_row)

    headers = [
        normalize_header(v)
        for v in rows
    ]

    for row in range(start_row, page.nrows):
        values = page.row_values(row)
        if not any(values):
            # an empty row
            continue
        if not all(v not in (None, u"", "")
                   for v in values[:required_columns]):
            # not well defined
            logging.warning("Skipping row %s of page %s; incomplete",
                            start_row + row, page.name)
            continue
        if any(v.startswith("#") if isinstance(v, str) else False
               for v in values[:required_columns]):
            # "commented out"
            logging.debug("Skipping row %s of page %s; commented",
                          start_row + row, page.name)
            continue
        columns = {
            # trying to clean up the values a bit; strings in excel
            # sheets tend to have trailing space and stuff.
            header.strip(): (value.strip()
                             if isinstance(value, str)
                             else value)
            for header, value in zip(headers, values)
            if header
        }
        yield columns


def format_attribute_property(name, value):
    "Apply special formatting needed to keep the TANGO db happy"
    if name in ("event_period", "archive_period"):
        # these apparently *must* be integer values, or TANGO will
        # complain about "unsupported format"
        return [str(int(value))]
    return [str(value)]


def split_on_char(string, separators=",; \n", maxsplit=0):
    "split string on any of a list of characters"
    return [x.strip()
            for x in re.split("[{}]+".format(separators),
                              string, maxsplit=maxsplit)]


def get_alias_from_tangoname(aliases, tangoname):
    # returns the first match
    for alias, name  in aliases.items():
        if name == tangoname:
            return alias
