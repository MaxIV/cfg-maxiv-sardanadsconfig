from __future__ import print_function
from itertools import chain
import json
import sys
import logging
import xlrd
import os

from dsconfig.tangodb import get_devices_from_dict
from tango.utils import CaselessDict, CaselessList

from sheets import process_motor_sheet, check_motor_sheet
from utils import (
    iter_rows,
    format_attribute_property,
    split_on_char,
    get_alias_from_tangoname,
)


def make_dsconfig(sardana_xls_name, motor_xlss=None, old_config={}):

    """
    Take a sardana config xls sheet and and produces a new dsconfig of
    the sardana setup described.

    A current dsconfig database dump of the target system can
    optionally be provided, and in that case we'll use it to try and
    keep the element ids unchanged as far as possible. This is not
    really necessary since AFAIK the ids are only used internally, but
    it makes the dsconfig diff a lot cleaner for inspection.

    It's also possible to supply any number of motorization sheets,
    which will then be harvested for icepap motors.

    """

    sardana_xls = xlrd.open_workbook(sardana_xls_name)

    # TODO:
    # - Missing stuff; there are several tabs we don't parse at all
    # - Error handling; mostly giving better feedback on broken configs
    # - Testing

    config = {}

    # going through the pages of the sheet and picking up relevant
    # info as we go.

    # **** Global ****
    info = {}
    page = sardana_xls.sheet_by_name("Global")
    for i in range(page.nrows):
        columns = page.row_values(i)
        if len(columns) == 2:
            key, value = columns
            if key:
                info[key] = value

    # Collect relevant devices from the old config
    # We'll use this to check if elements already have ids
    # and in that case keep it. Note that we need device
    # names and properties to be caseless!
    #
    # TODO: I think the ids only need to be unique per server,
    # so if we have several pools we don't need globally unique
    # ids. But perhaps that's easier to deal with anyway...
    old_devices = CaselessDict(
        {
            str(dev): {
                "properties": CaselessDict(
                    old_config.get("servers", {})[srv][inst][cls][dev].get(
                        "properties", {}
                    )
                )
            }
            for srv, inst, cls, dev in get_devices_from_dict(
                old_config.get("servers", {})
            )
            # We're going to assume that all pools will have names
            # starting with the name/code given in the global tab, e.g.
            # FemtoMAX -> FemtoMAX1, FemtoMAX2, ... or something.  This
            # is to prevent id collisions with other sardana installations
            # in the same TANGO database.
            # TODO: maybe add a switch to explicitly filter this, since
            # it's likely to break in some cases?
            if srv in ("MacroServer", "Pool")
            and inst.startswith((info["name"], info["code"]))
        }
    )

    # Collect all element ids from the old config
    # Any new element not present in the old config must be given
    # unique ids, so we need to keep track of used ids
    old_ids = set(
        [
            int(device["properties"].get("id", [-1])[0])
            for device in old_devices.values()
            if "id" in device["properties"] and device["properties"]["id"][0]
        ]
    )

    def make_id():
        # return a new unique id
        if old_ids:
            new_id = max(old_ids) + 1
        else:
            new_id = 1
        old_ids.add(new_id)
        return new_id

    def get_device_id(devicename):
        # find an id
        if devicename.lower() in old_devices:
            element_id = old_devices[devicename]["properties"]["id"][0]
            logging.debug(
                "Using existing id %s for device %s", element_id, devicename
            )
        else:
            element_id = str(make_id())
            logging.debug(
                "Creating new id %s for device %s", element_id, devicename
            )
        return element_id

    def get_instrument_id(poolname, instrumentname):
        # find an id inside InstrumentList property
        instrumentlist = []
        if poolname.lower() in old_devices:
            instrumentlist = old_devices[poolname.lower()]["properties"][
                "instrumentlist"
            ]
        if instrumentname in instrumentlist:
            # id is always the next value after instrument name
            instrument_index = instrumentlist.index(instrumentname) + 1
            element_id = instrumentlist[instrument_index]
            logging.info(
                "Using existing id %s for instrument %s",
                element_id,
                instrumentname,
            )
        else:
            element_id = str(make_id())
            logging.info(
                "Creating new id %s for instrument %s",
                element_id,
                instrumentname,
            )
        return element_id

    elements = {}
    controllers = {}

    # **** Servers ****
    #
    # The TANGO server(s) that will contain everything Typically
    # there's at least one Pool (containing all motors, equipment,
    # measurement groups, ...) and a MacroServer (containing
    # macroserver and doors)

    config["servers"] = servers = {}
    pools = {}
    aliases = {}

    # Protect the "EnvironmentDb" property from being removed
    # if it already exists (whatever that is for)
    PROTECTED_PROPERTIES = CaselessList(["EnvironmentDb"])
    logging.info("Processing sheet Servers")
    for columns in iter_rows(
        sardana_xls.sheet_by_name("Servers"), n_columns=9
    ):

        srv, inst = columns["server"].split("/")
        aliases[columns["name"]] = columns["tango_name"]

        device_name = columns["tango_name"]
        # check if there are any protected device properties already defined
        if device_name in old_devices:
            device = {
                "properties": {
                    prop: value
                    for prop, value in old_devices[device_name][
                        "properties"
                    ].items()
                    if prop in PROTECTED_PROPERTIES
                }
            }
        else:
            device = {"properties": {}}
        device["alias"] = columns["name"]

        # properties
        if columns["type"].lower() == "pool":
            if "path" in columns and columns["path"]:
                device["properties"]["PoolPath"] = columns["path"].split("\n")
            pools[columns["tango_name"]] = srv, inst
        elif columns["type"].lower() == "macroserver":
            if "path" in columns and columns["path"]:
                device["properties"]["MacroPath"] = columns["path"].split("\n")
            if "recorderpath" in columns and columns["recorderpath"]:
                device["properties"]["RecorderPath"] = columns["recorderpath"].split("\n")
            if "pools" in columns and columns["pools"]:
                device["properties"]["PoolNames"] = columns["pools"].split(
                    "\n"
                )

        # add the server
        server = {inst: {columns["type"]: {device_name: device}}}
        if srv in servers:
            servers[srv].update(server)
        else:
            servers[srv] = server

    # **** Doors ****
    #
    # These are entry points for clients, e.g. for running macros
    logging.info("Processing sheet Doors")
    for columns in iter_rows(sardana_xls.sheet_by_name("Doors"), n_columns=5):

        server, instance = columns["server"].split("/")
        devicename = columns["tango_name"]
        aliases[columns["name"]] = devicename
        element_id = get_device_id(devicename)
        device = {"alias": columns["name"], "properties": {"id": [element_id]}}
        if "Door" in servers[server][instance]:
            servers[server][instance]["Door"][devicename] = device
        else:
            servers[server][instance]["Door"] = {devicename: device}
        elements[columns["name"]] = device

    # **** Controllers ****
    #
    # These are the contact points between motors and actual hardware
    # Often there are more than one motors controlled by the same
    # piece of equipment, e.g. an IcePAP controller.

    controller_element_roles = {}
    logging.info("Processing sheet Controllers")
    for columns in iter_rows(
        sardana_xls.sheet_by_name("Controllers"), n_columns=8
    ):
        # find a device name
        if "devicename" in columns:
            if columns["devicename"].lower()=="automatic" or columns["devicename"].lower()=="":
                devicename = "controller/{class}/{name}".format(**columns).lower()
            else:
                devicename = columns["devicename"]
        else:
            devicename = "controller/{class}/{name}".format(**columns).lower()
        aliases[columns["name"]] = devicename
        # find an id
        element_id = get_device_id(devicename)

        device = {
            "alias": columns["name"],
            "properties": {
                "id": [element_id],
                "type": [columns["type"]],
                "library": [columns["file"]],
                "klass": [columns["class"]],
            },
        }

        if columns["properties"]:
            for prop in columns["properties"].split(";"):
                key, value = split_on_char(prop, ":=", 1)
                value_lines = value.encode("latin1").decode("unicode-escape").strip().split("\n")
                device["properties"][key.strip()] = value_lines

        if "elements" in columns and columns["elements"]:
            # This is only relevant for e.g. pseudomotors, and lists all
            # the actual child elements. This means we have to defer this
            # until after the elements are actually defined, because we need
            # their ids. See below.
            element_prop = None
            if columns["type"].lower() == "pseudomotor":
                element_prop = "motor_role_ids"
            elif columns["type"].lower() == "pseudocounter":
                element_prop = "counter_role_ids"
            if element_prop is not None:
                device["properties"][element_prop] = []
                controller_element_roles[columns["name"]] = [
                    el.strip() for el in split_on_char(columns["elements"])
                ]

        srv, inst = pools[columns["pool"]]
        if "Controller" not in servers[srv][inst]:
            servers[srv][inst]["Controller"] = {}
        servers[srv][inst]["Controller"][devicename] = device

        elements[columns["name"]] = device
        controllers[columns["name"]] = columns["pool"]

    # **** Instruments ****

    # AFAIK instruments are not functional in any way and are only useful
    # as a way to group several elements together under a descriptive name

    instruments = {}
    logging.info("Processing Instruments")
    for i, columns in enumerate(
        iter_rows(sardana_xls.sheet_by_name("Instruments"), n_columns=4)
    ):
        srv, inst = pools[columns["pool"]]
        pool = servers[srv][inst]["Pool"][columns["pool"]]
        name = columns["name"]
        poolname = columns["pool"]
        instrument_id = get_instrument_id(poolname, name)
        (
            pool["properties"]
            .setdefault("InstrumentList", [])
            .extend([columns["class"], columns["name"], str(instrument_id)])
        )
        instruments[columns["name"]] = instrument_id

    # **** Motors, PseudoMotors, IORegisters ****
    #
    # We allow some sheets to be missing here, leaving it up to the
    # user to arrange the sheet as he/she likes
    sheet_names = ["Motors", "PseudoMotors", "IORegisters"]  # ,..?

    for sheet_name in sheet_names:
        logging.info("Processing sheet %s", sheet_name)
        process_motor_sheet(
            sardana_xls,
            sheet_name,
            elements,
            aliases,
            instruments,
            controllers,
            pools,
            servers,
            get_device_id,
            xls_filename=sardana_xls_name,
        )

    if motor_xlss:
        # if the optional motorization sheet has been provided
        # we go through the Tango page and add all motors from there.
        # Note that it has different column names
        colmap = {
            "soft_limneg_units": "min_value",
            "soft_limpos_units": "max_value",
        }
        required_cols = ["name", "controller", "instrument", "axis", "ready"]
        optional_cols = ["unit", "min_value", "max_value"]

        for motor_xls in motor_xlss:
            logging.info("Processing optional motor sheet %s", motor_xls)
            book = xlrd.open_workbook(motor_xls)

            if check_motor_sheet(
                book,
                "Tango",
                colmap=colmap,
                xls_filename=motor_xls,
                required_cols=required_cols,
                optional_cols=optional_cols,
            ):
                process_motor_sheet(
                    book,
                    "Tango",
                    elements,
                    aliases,
                    instruments,
                    controllers,
                    pools,
                    servers,
                    get_device_id,
                    colmap=colmap,
                    xls_filename=motor_xls,
                    use_column="ready",
                )
            else:
                logging.warning(
                    "Optional motor sheet %s is invalid and not used",
                    motor_xls,
                )

    # TODO: CommunicationChannels,

    # **** Channels **** NAH
    channels = {}
    logging.info("Processing sheet Channels")
    channels_controller_roles = {}
    for columns in iter_rows(
        sardana_xls.sheet_by_name("Channels"), n_columns=20
    ):
        # find a device name
        devicename = columns["devicename"]

        if devicename == "Automatic":
            if columns["type"] in [
                "ZeroDExpChannel",
                "OneDExpChannel",
                "TwoDExpChannel",
                "CTExpChannel",
            ]:
                devicename = "expchan/{}/{}".format(
                    columns["controller"], int(columns["axis"])
                ).lower()
            elif columns["type"] in ["PseudoCounter"]:
                devicename = "pc/{}/{}".format(
                    columns["controller"], int(columns["axis"])
                ).lower()
            else:
                logging.warning(
                    "DeviceName for type %s not allowed to be 'Automatic', skipping: %s ",
                    columns["type"],
                    columns["name"],
                )
                continue
        aliases[columns["name"]] = devicename
        # find an id
        element_id = get_device_id(devicename)
        device = {
            "alias": columns["name"],
            "properties": {
                "id": [str(element_id)],
                "axis": [str(int(columns["axis"]))],
                "ctrl_id": [],
            },
        }
        if "controller" in columns and columns["controller"]:
            # This is only relevant for e.g. pseudomotors, and lists all
            # the actual controllers. This means we have to defer this
            # until after the motors are actually defined, because we need
            # their ids. See below.
            channels_controller_roles[columns["name"]] = [
                el.strip() for el in split_on_char(columns["controller"])
            ]
            if columns["type"].lower() == "pseudocounter":
                controller_alias = columns["controller"]
                controller_props = elements[controller_alias]["properties"]
                device["properties"]["elements"] = controller_props.get("counter_role_ids", [])

        if columns.get("attributes"):
            aps = device.setdefault("attribute_properties", {})
            for prop in columns["attributes"].split(";"):
                if not prop:
                    continue
                attr, value = prop.split(":", 1)
                if not value:
                    # Don't add empty properties (we never need those right?)
                    continue

                # TANGO wants memorized booleans to be lowercased (?)
                if value.lower() in ("true", "false"):
                    value = [value.lower()]
                else:
                    value = value.strip().split("\n")

                # TODO: perhaps do more sanity checking here, that
                # the values make sense, at least that the type is
                # correct. But that requires knowledge of all
                # possible motor device types.  Note: perhaps it
                # makes more sense to do this through the json
                # schema, to separate concerns a bit?

                aps[attr.strip()] = {"__value": value}

        elements[columns["name"]] = device
        controllers[columns["name"]] = columns["pool"]

        srv, inst = pools[columns["pool"]]
        if columns["type"] not in servers[srv][inst]:
            servers[srv][inst][columns["type"]] = {}

        servers[srv][inst][columns["type"]][devicename] = device

        elements[columns["name"]] = device
        controllers[columns["name"]] = columns["pool"]

    # **** Acquisition **** NAH
    acqusition = {}
    acquisition_channels_roles = {}
    logging.info("Processing sheet Acquisition")
    for columns in iter_rows(
        sardana_xls.sheet_by_name("Acquisition"), n_columns=20
    ):
        # find a device name
        devicename = columns["devicename"]
        if devicename == "Automatic":
            if columns["type"] in ["MeasurementGroup"]:
                devicename = "mntgrp/{}/{}".format(
                    get_alias_from_tangoname(aliases, columns["pool"]),
                    columns["name"],
                ).lower()
            else:
                logging.warning(
                    "DeviceName for type %s not allowed to be 'Automatic', skipping: %s ",
                    columns["type"],
                    columns["name"],
                )
                continue
        # find an id
        element_id = get_device_id(devicename)

        device = {
            "alias": columns["name"],
            "properties": {
                "id": [str(element_id)],
                "type": [columns["type"]],
                "elements": [],
            },
        }
        if "channels" in columns and columns["channels"]:
            # This is only relevant for e.g. pseudomotors, and lists all
            # the actual measurement group channels. This means we have to defer this
            # until after the motors are actually defined, because we need
            # their ids. See below.
            acquisition_channels_roles[columns["name"]] = [
                el.strip() for el in split_on_char(columns["channels"])
            ]

        srv, inst = pools[columns["pool"]]
        if columns["type"] not in servers[srv][inst]:
            servers[srv][inst][columns["type"]] = {}
        servers[srv][inst][columns["type"]][devicename] = device

        elements[columns["name"]] = device
        controllers[columns["name"]] = columns["pool"]

    # At this point we're able to fill in the controller role ids!
    logging.info("Looking up motor role ids")
    for alias, element_role_ids in controller_element_roles.items():
        try:
            logging.debug("Looking up ids for elements of %s", alias)
            ctrl_elements = [
                elements[role]["properties"]["id"][0]
                for role in element_role_ids
            ]
        except KeyError as e:
            logging.fatal(
                (
                    "Could not find element for motor role {} in controller {}!"
                    " Check that the motor is properly defined."
                ).format(e, alias)
            )
            sys.exit()
        # Note that we need to *modify* the list here, not replace it,
        # since we have references to it from all pseudo motors on
        # this controller. If this makes you feel uncomfortable,
        # repeat to yourself it's just a script and you should really
        # just relax!
        if "motor_role_ids" in elements[alias]["properties"]:
            elements[alias]["properties"]["motor_role_ids"][:] = ctrl_elements
        elif "counter_role_ids" in elements[alias]["properties"]:
            elements[alias]["properties"]["counter_role_ids"][:] = ctrl_elements

    # At this point we're able to fill in the acquisition channel role ids!
    logging.info("Looking up acquisition channel ids")
    for alias, acquisition_channel_role in acquisition_channels_roles.items():
        try:
            logging.debug("Looking up ids for elements of %s", alias)
            acquisition_channel = [
                elements[role]["properties"]["id"][0]
                for role in acquisition_channel_role
            ]
        except KeyError as e:
            logging.fatal(
                (
                    "Could not find element for acquisition channel {} in controller {}!"
                    " Check that the element is properly defined."
                ).format(e, alias)
            )
            sys.exit()
        # Note that we need to *modify* the list here, not replace it,
        # since we have references to it from all pseudo motors on
        # this controller. If this makes you feel uncomfortable,
        # repeat to yourself it's just a script and you should really
        # just relax!
        elements[alias]["properties"]["elements"][:] = acquisition_channel

    # At this point we're able to fill in the channel controller role ids!
    logging.info("Looking up channel controller ids")
    for alias, channel_controller_role in channels_controller_roles.items():
        try:
            logging.debug("Looking up ids for elements of %s", alias)
            channel_controller = [
                elements[role]["properties"]["id"][0]
                for role in channel_controller_role
            ]
        except KeyError as e:
            logging.fatal(
                (
                    "Could not find element for channel controller role {} in controller {}!"
                    " Check that the elements are properly defined."
                ).format(e, alias)
            )
            sys.exit()
        # Note that we need to *modify* the list here, not replace it,
        # since we have references to it from all pseudo motors on
        # this controller. If this makes you feel uncomfortable,
        # repeat to yourself it's just a script and you should really
        # just relax!
        elements[alias]["properties"]["ctrl_id"][:] = channel_controller

    # **** Parameters ****

    # Setting attribute configuration such as labels, limits...
    polling_periods = {}
    for columns in iter_rows(
        sardana_xls.sheet_by_name("Parameters"), n_columns=18
    ):
        if columns["element"] not in elements:
            logging.warning(
                "Skipping parameters for nonexistent element {}".format(
                    columns["element"]
                )
            )
            continue
        params = {
            header: format_attribute_property(header, columns[header])
            for header in [
                "label",
                "format",
                "description",
                "unit",
                "standard_unit",
                "display_unit",
                "min_value",
                "min_alarm",
                "min_warning",
                "max_value",
                "max_alarm",
                "max_warning",
                "delta_t",
                "delta_val",
                "abs_change",
                "rel_change",
                "event_period",
                "archive_abs_change",
                "archive_rel_change",
                "archive_period",
            ]
            # TODO: do we cover all possible attribute config props?
            if columns.get(header)
        }
        if params:
            element = elements[columns["element"]]
            if "attribute_properties" not in element:
                element["attribute_properties"] = {}
            element["attribute_properties"][columns["parameter"]] = params
        if columns["polling_period"] and columns["polling_period"] > 0:
            # Polling config is stored in a normal property, not in attribute
            # properties, for some reason.
            periods = polling_periods.setdefault(columns["element"], [])
            periods.extend(
                [columns["parameter"], str(int(columns["polling_period"]))]
            )
            element["properties"]["polled_attr"] = periods

    # Need to tweak the polling thread config if there are too
    # many polled attributes, otherwise we'll get annoying errors
    # about this all the time.
    # devs_per_thread = 1
    # if len(polling_periods) > devs_per_thread:
    #     n_threads = len(polling_periods) // devs_per_thread
    #     for srv, inst in pools.values():
    #         dserver_name = "dserver/{}/{}".format(srv, inst)
    #         dserver = (config["servers"][srv][inst]
    #                    .setdefault("dserver", {})
    #                    .setdefault(dserver_name, {}))
    #         devices = [aliases[el] for el in polling_periods.keys()]
    #         dserver["properties"] = {
    #             "polling_threads_pool_size": [str(n_threads)],
    #             "polling_threads_pool_conf": [
    #                 ",".join(devices[i*devs_per_thread:(i+1)*devs_per_thread])
    #                 for i in xrange(0, n_threads)
    #             ]
    #         }

    # We're just going to assign one polling thread to each motor
    polled_motors = [aliases[m] for m in polling_periods]
    for srv, inst in pools.values():
        if "Motor" not in servers[srv][inst]:
            continue
        motors = [
            m for m in servers[srv][inst]["Motor"].keys() if m in polled_motors
        ]
        n_threads = len(motors)
        if n_threads > 0:
            dserver_name = "dserver/{}/{}".format(srv, inst)
            dserver = (
                config["servers"][srv][inst]
                .setdefault("dserver", {})
                .setdefault(dserver_name, {})
            )
            # Note: we must be careful here, the properties must match
            # so that the pool size is the same as the number of lines
            # in the pool conf. Otherwise TANGO will segfault (at least
            # version 9.2.5)... :(
            assert (
                len(motors) == n_threads
            ), "Number of polling threads is inconsistent; FIXME!"
            dserver["properties"] = {
                "polling_threads_pool_size": [str(n_threads)],
                "polling_threads_pool_conf": motors,
            }

    return config


if __name__ == "__main__":

    from optparse import OptionParser

    # import xlrd

    usage = "%prog [options] <sardana sheet.xlsx> [<motor sheet.xlsx>] [-u <dsconfig_dump.json>]"
    parser = OptionParser(usage)
    parser.add_option(
        "-u",
        "--use-dump",
        dest="dump",
        help="take sardana IDs from the given dsconfig dump file",
    )
    parser.add_option(
        "-d",
        "--debug",
        action="store_true",
        dest="debug",
        default=False,
        help="print debug messages (to stderr)",
    )

    options, args = parser.parse_args()
    if len(args) == 0:
        logging.fatal(
            "You need to provide at least an excel sheet as argument"
        )
        sys.exit()

    if options.debug:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)

    sardana_xls = args[0]

    motor_sheets = args[1:]

    if options.dump:
        with open(options.dump) as f:
            current_config = json.load(f)
    else:
        current_config = {}

    config = make_dsconfig(sardana_xls, motor_sheets, current_config)

    # verify that the result is well formatted
    from dsconfig.formatting import validate_json

    try:
        validate_json(config)
    except Exception as e:
        logging.fatal("Validation error, please report this as an issue!")
        raise (e)

    from jsonschema import validate

    logging.info("Validation output")
    try:
        filename = (
            os.path.dirname(os.path.realpath(__file__))
            + "/test/dsconfig_schema.json"
        )
        schema_file = open(filename)
        schema_data = json.loads(schema_file.read())
        validate(config, schema_data)
    except Exception as e:
        logging.warning("Validation error, output not according to schema!")
        logging.warning(e)
    print(json.dumps(config, indent=4, sort_keys=True))
