"""
Quick way to compare a motor sheet with the current Sardana config.
Does not modify the TANGO db in any way!

Output is friendly to sorting, filtering, etc with commandline tools:

- global sort
$ python check_motor_sheet.py motors.xlsx | sort

- show only changed attributes
$  ... | grep "Change_attr"

- sort on controller and axis (columns 2 and 3):
$  ... | sort -k2,2 -k3,3n  # <- note the trailing "n"!


Notes
=====

- Sometimes it may appear that the script reports the wrong actions,
  such as adding attributes (e.g. Sign) though they already have values
  according to TANGO. The issue here is that if an attribute has never
  been written, it still has a *default* value in the device (e.g. "1"
  for Sign). However, the default is not written to the database,
  which is the only thing this script (currently) checks. Once
  the attributes have been written to the DB this issue will not appear
  again.

- The Motor SAT file has slightly change. 
  Use the constant READY_V2 for Femtomax, Softimax ... if you want to process this information

"""

from collections import defaultdict
import logging
from sheets import process_motor_sheet

from dsconfig.tangodb import get_servers_with_filters, get_devices_from_dict
from dsconfig.utils import green, red, yellow
import PyTango as tango
from PyTango.utils import CaselessDict

READY_V2 = u"ready_n_or_blank_dont_take_the_data_here"
READY_V1 = u"Ready"

def check_motor_sheet(xls, verbose=False, use_ready=None, use_colmap=False,
                      server_filter="Pool/*"):
    db = tango.Database()
    dbproxy = tango.DeviceProxy(db.dev_name())

    servers = get_servers_with_filters(dbproxy, server=server_filter)
    new_servers = defaultdict(dict)
    elements = CaselessDict()
    controllers = CaselessDict()
    pools = CaselessDict()

    for srv, inst, cls, dev in get_devices_from_dict(servers):
        if cls == "Controller":
            ctrl = servers[srv][inst][cls][dev].to_dict()
            if "alias" in ctrl:
                controllers[ctrl["alias"]] = None
                elements[ctrl["alias"]] = ctrl
            else:
                logging.warning("No alias found for controller %s?!", dev)
        if cls == "Pool":
            if "" in pools:
                print("Warning, ignoring pool: {}/{}".format(pools[""][0], pools[""][1]))
                print("To avoid this, use the '--pool' argument to specify which pool to check against.")
            pools[""] = srv, inst
            new_servers[srv][inst] = {}
            instrumentlist = db.get_device_property(dev, "InstrumentList")["InstrumentList"]
    print("Checking against pool: {}/{}".format(pools[""][0], pools[""][1]))
    aliases = {}
    instruments = {}
    instr_names = instrumentlist[1::3]
    instr_ids = instrumentlist[2::3]
    for name, id in zip(instr_names, instr_ids):
        instruments[name] = id

    colmap_old = {
        "device": "devicename",
        "controller_sardana": "controller",
        "axis_name_sardana": "name",
        "axis_nr": "axis",
        "attribute_chain": "attributes",
    }
    colmap = {
        "soft_limneg_units": "min_value",
        "soft_limpos_units": "max_value",
    }
    colmap = colmap_old if use_colmap else colmap
    
    use_column = "ready" if use_ready else None

    process_motor_sheet(xls, "Tango",  elements, aliases,
                        instruments, controllers, pools, new_servers,
                        lambda x: "0", colmap=colmap,
                        use_column=use_column)

    ctrl_by_id = {
        elements[alias]["properties"]["id"][0]: elements[alias]
        for alias in controllers
    }

    def uncolored(text):
        return text

    def print_action(color, *action):
        widths = [15, 20, 6, 20, 25, 10, 10, 10, 10, 10, 10, 10, 10]
        print(color("".join("{{:{}}}".format(w).format(str(item)) for item, w in zip(action, widths))))

    print_action(uncolored, "Action", "Controller", "Axis", "Alias", "Prop/Attr Name", "Change")
    
    for server, instances in new_servers.items():
        for instance, classes in instances.items():
            for clss, devices in classes.items():
                for devname, device in devices.items():

                    alias = device["alias"]
                    axis = device["properties"]["axis"][0]
                    ctrl = ctrl_by_id[device["properties"]["ctrl_id"][0]]
                    controller = ctrl["alias"]
                    try:
                        fullname = db.get_device_from_alias(alias)
                        try:
                            ctrl_id = db.get_device_property(fullname, "ctrl_id")["ctrl_id"][0]
                        except:
                            ctrl_id = "<no id>"
                    except:
                        ctrl_id = None

                    if ctrl_id is not None and ctrl_id != ctrl["properties"]["id"][0]:
                        print
                        logging.warning("Alias \"{}\" already exists for device \"{}\"".format(alias, fullname))

                    if ctrl["properties"]["klass"][0].lower() != "icepapcontroller":
                        logging.debug("Ignoring non-icepap controller {}".format(alias))
                        continue

                    old_device = CaselessDict(servers.get(server, {})
                                              .get(instance, {})
                                              .get(clss, {})).get(devname, {})

                    if not old_device:           
                        print_action(green, "Add_device", controller, axis, alias, devname)
                        continue
                    if device["alias"].lower() != old_device["alias"].lower():
                        print_action(yellow, "Change_alias", controller, axis, old_device["alias"] + " -> " + alias)

                    # Properties (don't think these will ever actually change...)
                    if "properties" in device:
                        for propname, propvalue in device["properties"].items():
                            if propname.lower() in ("id",):
                                continue
                            if propname in CaselessDict(old_device.get("properties", {})):
                                old_propvalue = old_device.get("properties", {}).get(propname)
                                
                                if propvalue != old_propvalue:
                                    print_action(yellow, "Change_prop", controller, axis, alias,
                                                 propname, str(old_propvalue) + " -> " + str(propvalue))
                                elif verbose:
                                    print_action(green, "Same_prop", controller, axis, alias,
                                                       propname, propvalue)
                            else:
                                print_action(green, "Add_prop", controller, axis, alias,
                                             propname, propvalue)
                    if "properties" in old_device:
                        for propname, propvalue in old_device["properties"].items():
                            if propname.startswith("__"):
                                continue
                            if propname not in CaselessDict(device.get("properties", {})):
                                print_action(red, "Remove_prop", controller, axis, alias, propname)

                    # Attributes (this is the real config stuff)
                    if "attribute_properties" in device:
                        old_attrs = CaselessDict(old_device.get("attribute_properties", {}))
                        for attribute, attrprops in device["attribute_properties"].items():
                            old_attr_props = old_attrs.get(attribute, {})
                            for prop, val in attrprops.items():
                                #if prop != "__value_ts":
                                if prop.lower() in ["__value", "unit", "min_value", "max_value"]:
                                    prop_label = "" if prop == "__value" else "/"+prop
                                    if prop not in old_attr_props:
                                        print_action(green, "Add_attr", controller, axis,
                                                     alias, attribute+prop_label,
                                                     val[0])
                                    elif old_attr_props.get(prop) != val:
                                        print_action(yellow, "Change_attr", controller, axis,
                                                     alias, attribute+prop_label,
                                                     old_attr_props[prop][0] + " -> " + val[0])
                                    elif verbose:
                                        print_action(green, "Same_attr", controller, axis, alias,
                                                     attribute+prop_label, attrprops[prop][0])
                    if "attribute_properties" in old_device:
                        attrs = CaselessDict(device.get("attribute_properties", {}))
                        for attribute, old_attrprops in old_device["attribute_properties"].items():
                            logging.debug("attribute {}, new props {}, old props {}".format(attribute, attrs.get(attribute, {}), old_attrprops.to_dict()))
                            if attribute.lower() not in ("dialposition", "poweron", "encoder",
                                                             "posinpos", "encencin"):
                                attr_props = attrs.get(attribute, {})
                                for prop, val in old_attrprops.items():
                                    if prop in ["__value", "unit", "min_value", "max_value"]:
                                        prop_label = "" if prop == "__value" else "/"+prop
                                        if prop not in attr_props:
                                            print_action(red, "Remove_attr", controller, axis,
                                                         alias, attribute+prop_label,
                                                         val[0])

    for server, instances in servers.items():
        for instance, classes in instances.items():
            for clss, devices in classes.items():
                if clss != "Motor":
                    continue
                for devname, device in devices.items():
                    new_device = new_servers.get(server, {}).get(instance, {}).get(clss, {}).get(devname, {})
                    if not new_device:
                        if "alias" in device:
                            alias = device["alias"]
                        else:
                            # in some unusual cases, the alias may have been removed
                            alias = "<no alias>"
                        print_action(red, "Remove_device", "", "", alias, devname)


if __name__ == "__main__":

    import argparse
    import xlrd
    parser = argparse.ArgumentParser(description='Check a motor sheet against database')

    parser.add_argument(
        "-v", "--verbose", action="store_true", dest="verbose", default=False,
        help="display unchanged properties/attributes")
    parser.add_argument(
        "-u", "--unready", action="store_false", dest="ready", default=True,
        help="Also check columns that don't have the 'ready' column filled in")
    parser.add_argument(
        "-c", "--colmap", action="store_true", dest="colmap", default=False,
        help="Translate old column names")
    parser.add_argument(
        "-p", "--pool-server", dest="pool_server", default="Pool/*",
        help="Only check against Pool servers matching the given pattern.")
    parser.add_argument(
        "-d", "--debug", default=False, action="store_true",
        help="Display more messages about what's going on"
    )
    parser.add_argument('motorsheet')

    options = parser.parse_args()

    if options.debug:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)
    check_motor_sheet(xlrd.open_workbook(options.motorsheet), verbose=options.verbose,
                      use_ready=options.ready, use_colmap=options.colmap,
                      server_filter=options.pool_server)
